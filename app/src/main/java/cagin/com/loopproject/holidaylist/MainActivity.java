package cagin.com.loopproject.holidaylist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import cagin.com.loopproject.CirclePagerIndicatorDecoration;
import cagin.com.loopproject.R;

public class MainActivity extends AppCompatActivity {
    private List<HolidayModel> holidayList;
    private HolidayRecyclerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createHolidayList();
        setRecycler();
    }

    private void createHolidayList(){
        holidayList=new ArrayList<>();
        HolidayModel holidayModel;
        holidayModel= new HolidayModel(getResources().getString(R.string.title_ibiza),getResources().getString(R.string.desc_ibiza),
                getResources().getDrawable(R.drawable.ibiza),getResources().getString(R.string.first_look_ibiza));
        holidayList.add(holidayModel);
        holidayModel = new HolidayModel(getResources().getString(R.string.title_santorini),getResources().getString(R.string.desc_santorini),
                getResources().getDrawable(R.drawable.santorini),getResources().getString(R.string.first_look_santorini));
        holidayList.add(holidayModel);
        holidayModel = new HolidayModel(getResources().getString(R.string.title_cesme),getResources().getString(R.string.desc_cesme),
                getResources().getDrawable(R.drawable.cesme),getResources().getString(R.string.first_look_cesme));
        holidayList.add(holidayModel);
        holidayModel = new HolidayModel(getResources().getString(R.string.title_borabora),getResources().getString(R.string.desc_borabora),
                getResources().getDrawable(R.drawable.borabora),getResources().getString(R.string.first_look_borabora));
        holidayList.add(holidayModel);
        holidayModel = new HolidayModel(getResources().getString(R.string.title_miami),getResources().getString(R.string.desc_miami),
                getResources().getDrawable(R.drawable.miami),getResources().getString(R.string.first_look_miami));
        holidayList.add(holidayModel);
    }
    private void setRecycler(){
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerViewHoliday = (RecyclerView) findViewById(R.id.recyclerHolidayList);
        recyclerViewHoliday.setLayoutManager(layoutManager);
        mAdapter = new HolidayRecyclerAdapter(holidayList,this);
        recyclerViewHoliday.setAdapter(mAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewHoliday);
        CirclePagerIndicatorDecoration circlePagerIndicatorDecoration = new CirclePagerIndicatorDecoration();
        recyclerViewHoliday.addItemDecoration(circlePagerIndicatorDecoration);
    }
}
