package cagin.com.loopproject.holidaylist;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cagin.com.loopproject.R;
import cagin.com.loopproject.holidaydetail.HolidayDetailActivity;

public class HolidayRecyclerAdapter extends RecyclerView.Adapter<HolidayRecyclerAdapter.MyViewHolder> {

    private final static int FADE_DURATION = 1000;
    private List<HolidayModel> holidayList;
    private Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title, firstLook;
        public ImageView holidayImage;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.textViewTitle);
            holidayImage = (ImageView) view.findViewById(R.id.imageViewHoliday);
            firstLook = (TextView) view.findViewById(R.id.textViewFirstLook);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Activity activity = (Activity) ctx;
            Intent intent = new Intent(ctx, HolidayDetailActivity.class);
            intent.putExtra("holidayItem", holidayList.get(getAdapterPosition()));
            ctx.startActivity(intent);
            activity.overridePendingTransition(R.anim.entry_anim, R.anim.exit_anim);
/*
            ActivityOptions activityOptions;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                activityOptions = ActivityOptions.makeSceneTransitionAnimation(
                        (Activity) ctx, holidayImage, "imageViewTransition");
                ctx.startActivity(intent, activityOptions.toBundle());
              //  activity.overridePendingTransition(R.anim.entry_anim, R.anim.exit_anim);
            }else{
                ctx.startActivity(intent);
              //  activity.overridePendingTransition(R.anim.entry_anim, R.anim.exit_anim);
            }*/

        }
    }

    public HolidayRecyclerAdapter(List<HolidayModel> holidayList, Context context) {
        this.holidayList = holidayList;
        this.ctx=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_holiday, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HolidayModel holidayModel = holidayList.get(position);
        holder.title.setText(holidayModel.getTitle());
        holder.firstLook.setText(holidayModel.getFirstLook());
        holder.holidayImage.setImageDrawable(holidayModel.getResource());
        setFadeAnimation(holder.itemView);
    }
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }
    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return holidayList.size();
    }
}
