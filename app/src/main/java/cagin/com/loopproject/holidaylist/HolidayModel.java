package cagin.com.loopproject.holidaylist;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;


public class HolidayModel implements Parcelable {
    private String title;
    private String description;
    private Drawable resource;
    private String firstLook;

    protected HolidayModel(Parcel in) {
        title = in.readString();
        description = in.readString();
        firstLook = in.readString();
    }

    public static final Creator<HolidayModel> CREATOR = new Creator<HolidayModel>() {
        @Override
        public HolidayModel createFromParcel(Parcel in) {
            return new HolidayModel(in);
        }
        @Override
        public HolidayModel[] newArray(int size) {
            return new HolidayModel[size];
        }
    };

    public String getFirstLook() {
        return firstLook;
    }

    public void setFirstLook(String firstLook) {
        this.firstLook = firstLook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Drawable getResource() {
        return resource;
    }

    public void setResource(Drawable resource) {
        this.resource = resource;
    }

    public HolidayModel(String title, String description, Drawable resource, String firstLook) {
        this.title = title;
        this.description = description;
        this.resource = resource;
        this.firstLook = firstLook;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(firstLook);
    }
}
