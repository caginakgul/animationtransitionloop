package cagin.com.loopproject.holidaydetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import cagin.com.loopproject.R;
import cagin.com.loopproject.holidaylist.HolidayModel;
import cagin.com.loopproject.MyBounceInterpolator;

public class HolidayDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        setData();
    }
    private void setData(){
        Intent i = getIntent();
        HolidayModel holidayItem = getIntent().getParcelableExtra("holidayItem");
        TextView textViewTitle = (TextView)findViewById(R.id.textViewDetailTitle);
        TextView textViewDescription = (TextView)findViewById(R.id.textViewDescription);
        ImageView imageViewHoliday = (ImageView)findViewById(R.id.imageViewDetail);
        textViewTitle.setText(holidayItem.getTitle());
        textViewDescription.setText(holidayItem.getDescription());

        switch (holidayItem.getTitle()){
            case "Ibiza" :
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.ibiza));
                break;
            case "Bora Bora" :
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.borabora));
                break;
            case "Santorini" :
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.santorini));
                break;
            case "Cesme" :
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.cesme));
                break;
            case "Miami" :
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.miami));
                break;
            default:
                imageViewHoliday.setImageDrawable( getResources().getDrawable(R.drawable.santorini));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.close_enter, R.anim.close_exit);
    }
    public void didTapButton(View view) {
        Button button = (Button)findViewById(R.id.button);
        if(button.isSelected())
            button.setSelected(false);
        else
            button.setSelected(true);
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        button.startAnimation(myAnim);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        button.startAnimation(myAnim);
    }
}
